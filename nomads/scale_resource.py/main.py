from scale import MusicScale, MusicScaleType
from capo import Capo
from tuning import GuitarTuning 


def choose_tuning():
    guitar_tuning = int(input("""                                                  1. Standard,
                                                  2. DADGBD
                                                  3. DADGAD
                            >  """))
    return guitar_tuning

def get_fret_number():
    capo_fret = int(input("Which fret would you like the capo on? "))
    return capo_fret

def establish_notes():
       for string in zipped_string:
            zipped_string_list = list(zipped_string)
            return zipped_string_list

   

def choose_scale():
    scale_choice = int(input("""Which scale ? 1. Chromatic,
                                                                        2.  Major
                                                                        3. Natural Harmonic Minor
                                                                        4. Pentatonic
                                                """))
    return scale_choice

def use_scale_choice():
    if scale_choice == "1":
        return chromatic_scale
    elif scale_choice == "2":
        return major_scale
    elif scale_choice == "3":
        return natural_harmonic_minor_scale
    else:
        return pentatonic_scale

guitar_tuning = choose_tuning()

if guitar_tuning == "1":
    standard = True
elif guitar_tuning == "2":
    drop_d = True
else:
    dadgad = True

capo_fret_number = get_fret_number()
scale_choice = choose_scale()
scale_use = use_scale_choice() 

print(capo_fret_number)
print(scale_choice)
print(scale_use)

#notes = establish_notes()
#
#
#if notes in zipped_string_list == True:
#    if capo_fret == 0:
#        print(notes[0:14])
#    elif capo_fret == 1:
#        print(notes[1:15])
#    elif capo_fret == 2:
#        print(notes[2:16])
#    elif capo_fret == 3:
#        print(notes[3:17])
#    elif capo_fret == 4:
#        print(notes[4:18])
#    elif capo_fret == 5:
#        print(notes[5:19])
#    elif capo_fret == 6:
#        print(notes[6:20])
#    elif capo_fret == 7:
#        print(notes[7:21])
#    else:
#        print(notes[8:22]) 
#else:
#    print('---')    
#
