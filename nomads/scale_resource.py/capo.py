from scale import MusicScale, MusicScaleType
from tuning import GuitarTuning

class Capo():
    def __init__(self, fret, string, zipped_string):
        self.fret = fret
        self.string = string
        self.zipped_string = zipped_string

    def establish_standard_tuning(self):
        for i in standard:
            string = notes[i:i + 15]
            zipped_string = zip(fret, strings)
            return zipped_string

    def establish_drop_d(self):
        for i in drop_d:
            string = notes[i:i + 15]
            zipped_string = zip(fret, strings)
            return zipped_string

    def establish_dadgad(self):
        for i in dadgad:
            string = notes[i:i + 15]
            zipped_string = zip(fret, strings)
            return zipped_string
    
   

    
    
