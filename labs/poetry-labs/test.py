import requests
#
response = requests.get(
    'https://api.github.com/search/repositories',
    params = {'q': 'requests+language:python' },
    headers = {'Accept': 'application/vnd.github.v3.text-match+json'},
)

j_response = response.json()
repo = j_response['items'][0]
print(f'Repository name: {repo["name"]}')
print(f'Repository description: {repo["description"]}')
print(f'Text matches: {repo["text_matches"]}')

