import sys

def print_greeting():
    print(str("Welcome to my cli-game.").center(90))
    print(str("****************************************************").center(90))
    print(str("""        function enter_room gives the player the opportunity to choose between two doors,
        if player chooses the Left door, then they can choose to 'stay' and 'look around',
        or to leave and choose which door they want to enter in. If player 'looks around',
        they find a hidden sword. They then have the choice to take it or not. Regardless
        of whether or not they take the sword, they can then choose which room where they
        find out their fate.
    """))
    return input("Enter Your Name: > ").capitalize()
player_1 = print_greeting()

print(f"Hello {player_1}!")

def enter_room():
    
    play = input("Enter A Room? (L or R): > ").upper()
    if play == 'L':
        player_chooses_left()
    elif play == 'R':
        player_chooses_right()

def player_leaves():
    enter_room()

def player_play_again():
    play_again = input("Would you like to play again (Y or N)? > ").upper()
    if play_again == 'Y':
        enter_room()
    sys.exit

def player_wins():
    print("You just cut the dragon's head off. Congratulations!")
    player_play_again()

def player_loses():
    print("You just became a Big Mac for the Dragon. You lose, sucker!")
    player_play_again()

def player_chooses_left():
    empty_room = input("The room appears to be empty. Would you like to (S)tay and 'look-around', or (L)eave? ").upper()
    if empty_room == 'S':
        print(f"Congrats {player_1}, you just found a sword. Do you want to keep it (Y or N?")
        sword = input(": > ").upper()
        if sword == 'Y':
            player_wins()
        else:
            enter_room()
    enter_room()

def player_chooses_right():
    dragon = input("There's a dragon. Do you want to (S)tay and fight, or do you want to (L)eave? > ").upper()
    if dragon == 'S':
        player_loses()
    enter_room()
    
enter_room()


