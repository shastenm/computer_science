# Data Structures#

## STRINGS##

1. String Indexes

**VOCABULARY**
- **string:** a string of characters.
- **mnemonic:** an excepted protocol among developers pertaining to the conventions of naming variables and methods.
- **string index:** A number method that represents each individual character in a string. They are represented by using the '[]' characters after the variable.
- **string slice:** a segment of a string specified by a range of indices.
- **immutable:** Cannot be changed directly.  

```
car = 'Honda'
```

In the example above, you have car which is a variable, and that is given the value of the string, 'Honda'. The string could literally be anything, such as, 'asdwoeiuraflsk'. If this was the variable, I would pull my hair out and ask that it be way more mnemonic in your naming conventions, but the above example of 'asdwoeiuraflsk' could absolutely be a string if one were completely demented or tried to code while on high on illicit substances. I plea the fifth.  

So, how would you access these indexes? It's really simple.

Since, I live in Mexico, I have a really good string to share, it's the village of Parangaricutirimicuaro (located in Michoacan, Mexico) which serves as a tongue-twister in Spanish.
```
village = 'Parangaricutirimicuaro'
pletter = village[0]
print(pletter)
``` 
The index [0] will return the letter 'P' which is the first letter in the string. 
```
String: P	a	r	a	n	g	a	r	i	c	u	t	i	r	i	m	i	c	u	a	r	o
-----------------------------------------------------------------------------------------------
Index:	0	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20 	21
```
So, here we can see that we have a range of indexes between 0 and 21, but what happens when we use the len() function to this string. So, if we piggy-back off of the last example, we can use the pletter variable.
```
print(len(pletter))
```
This gives an output of 22 because their was 22 characters in this string, but this has a discrepancy of **one** as you are about to see.
```
village = 'Parangaricutirimicuaro'
length = len(village)
last = village[length]
```
If you were casually following along, you would have thought that this would give you the last letter, except it gives you an *IndexError: string index out of range*. The fact is, **village[22]** doesn't exist, so what can you do?

A couple of ways of dealing with this would be:
```
last = village[length-1]
```
There has to be an easier way to get the last character of a string, right? Yes. You do this by using the -1 index.
```
last = village[-1]
```
The **-1 index** always returns the last character of a string ('o'), and **-2 index** returns the second to last ('r'), etc.

It is possible to run a string through a loop, and you will probably have to do something like this at some point in your programming future.
```
village = 'Parangaricutirimicuaro'
index = 0
while index < len(village):
   letter = village[index]
   print(letter)
   index += 1
```
This will print out every letter in the string (Parangaricutirimicuaro), individually. It returns it such a fashion almost as if you were concatenating each individual letter right next to each other. This is kind of a long about way of doing this though. Most of the time, a **for loop** would be more appropriate.

```
for char in village:
	print(char)
```

Now, let's use some things we have learned and going to learn very shortly to concatenate a string. I'm going to us another town in Jalisco Mexico named Yahualica for a string.
```
town = 'Yahualica!'
fl = town[1].capitalize()
r = town[2:5:2]
l = town[-1]
print(fl + r + l + ",", "I finally caught you.")
```
This is just a fun exercise in practicing concatenation of a string. If you did it correctly, it should return, "Aha!, I finally caught you." The variable *fl* (first letter) has an index of 1 which would return an 'a', but I use the capitalize method to capitalize it. The, the variable *r* (rest), has and index that starts at two. Then, it goes up to but not including the 5 in intervals of two. Finally, we have the variable *l* (last) which prints out the last character, and I print them all together with a sentence fragment.

```
r	=	town[2	:	5	:	2] 
---------------------------------
		town[start:stop:interval]
```
The interval 2 just says that we are choosing every other character. You could choose a 3 for instance, and it would render every third number, etc.

Let's take a look at concatenating this string further by evaluating the following code.
```
town = 'Yahualica!'
string = town[1:-1:3]
string1 = town[1:20:3]
length = len(town)
string2 = town[-2:0:-3]

print('1.', string)
print('2.', string1)
print('3.', length)
print('4.', string2)

Output:
1. aac
2. aac
3. 10
4. alh
```
The first string is pretty straight-forward. Choose index 1, go to the end string, output every 3rd character. But, I bet you got stumped on the second one because when you look at the length in the 3rd output, there are only 10 characters. I thought it would output an IndexError. I guess not!

This brings us to our last string. It basically says to start at the second to last character of the string, and work backwards to 0 in intervals of -3. To work backwards, you have to use a negative interval.

This all seems really complicated, so what if I just wanted to take a section out of a string? Can I do that? Absolutely.

For this example, I am going to use the biggest public Park in all of Mexico and maybe the entire world, Chapultepec. It's located in Mexico City, and it is magnificent. It's approximately 5 times the size of Central Park in New York and it sports 5 museums and many other things that I can't think of off the top of my head. One would do well to put seeing Parque de Chapultepec on their bucket list, they won't be disappointed. 

```
park = 'chapultepec'
string = park[0:5]
string1 = park[6:]
string2 = park[:6]
```
If you print these out, you get 'chapu', 'tepec', and 'chapul' respectively. So, what is going on here?
The first string, we print out index 0 up to, but not including index 5. In other words, it returns indexes 0 to 4. The second string, it return from index 6 to the end of the string. And finally, the last string returns all the indexes up to but not including the 6th index, indexes 0 through 5.

Using the same string, we can do something else kinda interesting.
```
string = park[0::3]
```
This returns the output of 'cpte' It basically goes through and picks out every third index.

**Strings Are Immutable**

Strings are de facto immutable, what does that mean? In layman's terms, it means that it cannot be changed directly. For this example, I am going to include one of my favorite fruits from Mexico, *maracuya* or passion fruit, to demonstrate this fact.

```
>>> fruit = 'maracuya'
>>> fruit[0] = 'y'
TypeError: 'str' object does not support item assignment
```
 If you didn't get it, I was attempting to change the first letter which the prompt screamed at me saying that I was this naughty boy and boy, they don't know the half of it. What you can do instead:

```
>>> fruit = 'maracuya'
>>> n.fruit = 'y' + fruit[1:]
yaracuya
```

To conclude this, it is possible to slice and dice your string anyway you see fit, but you cannot change the actual string directly. Another cool thing you can do is:
```
fruit = 'maracuya'
count = 0 
for letter in fruit:
	if letter == 'a':
		count += 1
print(count)
```
This should through the loop individually, one by one, and count each 'a' that is in the variable 'fruit'. This should return an output of 3.

Another cool thing, you have seen me do this, but you may not realize how cool this is:
```
'count = count + 1' is the same as 'count += 1'
```
Just an FYI in case you were wondering. Now, back to the subject at hand.

**The 'in' Operator**
The word 'in' is a boolean operator that takes in two *strings* and *returns* either *True* if the fragmented part of the first string appears in the second string or *False* if it does not. 
```
>>>'a' in 'maracuya'
True
>>>'ray' in 'maracuya'
False
```

**Comparing Strings**

This is an interesting area that we should explore quite a bit, but I am going to explore this just ever so briefly for the moment. For this example, I am going to employ other tropical fruits that I find in Mexico.

```
fruit1 = 'yaca'
fruit2 = 'maracuya'
fruit3 = 'pitaya'
fruit4 = 'carambola'
fruit5 = 'rambutan'
afruit = input('Enter in a string: ').lower()

if afruit == fruit2:
	print(afruit, "is equal to", fruit2 + ".")
elif afruit > fruit2:
	print(afruit, "is greater than", fruit2 + ".")
elif afruit < fruit2:
	print(afruit, "is less than", fruit2 + "."
```
As you can clearly see, it is possible to use comparison symbols to compare strings alphabetically. Lower-cased are considered greater than upper-cased letters. With pretty much the same variables above, let's just modify *'afruit'* and *'fruit2'* to start with a capital letter, *'Maracuya'*. 

```
fruit2 = 'Maracuya'
afruit = input('Enter in a string: ')
```
Now, I like to test things. In theory, maracuya is greater than carambula, but it is the same with the 'M' in 'Maracuya' *capitalized*? No, carambola is greater than 'Maracuya'. Interestingly enough, you can employ the '__eq__' method to this string as well. Using the above strings, we can demonstrate this very easily.
```
if afruit.__eq__(fruit4):
	print('True')
else:
	print('False')
```
How cool is that? You can make *afruit* into a *class method* which is something you will learn about later.

**String Methods**

An example of Python *objects* would be *strings*. An object has both data and methods which serve as functions. As you saw in the previous example where there was a class method was used to define afruit, this is a list of all the methods that are available to a string.

Here is an output of the dir() function being used.

![Photo 1](file:///home/tuxer/Documents/dir.png)

As you probably noticed if you have spent any time working with Python is that it has listed a lot of so-called 'Magic Methods' or 'Dunder Methods' which is short for *'Double Underscore'*. There are some examples where it can be used outside of a class when using a string, but most of them are designated specifically for the purpose of using within a *class*. 

An example of where it can be used outside of a class would be something like what we just saw or using something like the __add__() function which just basically concatenates one string to another.

```
>>> something = 'Some crazy string.'
>>> new_something = something.__add__(something)
>>> print(new_something)
Some crazy string.Some crazy string.
```

If you use the majority of these outside of a class, you will most likely get an *AttributeError* or a *TypeError*. It will become more obvious as to why when you start using *classes*. Mainly, we should concentrate on some of the more common methods of manipulating strings.
```
# capitalize() - capitalizes the first letter in the string.

>>> name = input('Enter A Name: ').capitalize()
> shasten
print(name)
Shasten

# casefold() -- changes upper to lower-case letters.
>>> name = 'SHASTEN'
>>> print(name.casefold())
shasten

#center() -- centers the text with some input parameters
>>> a = 'GAME'
>>> b = a.center(20, "-")
>>> print(b)
--------GAME--------

#count() -- counts the number of specific characters in a string.
>>> town = 'Parangaricutirimicuaro'
>>> print(town.count("a"))
4 
```
In a moment, I am going to do a few examples of the *find()* method to show how cool it is for things like scripting purposes. The *format()* method have been replaced by f-strings (much cooler!). The *format_map()* method is still used from time to time with a dictionary which I will demonstrate fairly soon.

The, what I call is-methods, *isalpha()*, *isalnum()*, *isdigin()*, etc return a boolean expression and they are mostly pretty self-explanatory. I really don't see a point in re-hashing those out.

```
# join(iterable) -- joins (iterates) a string to another string concatenation.
>>> a = '-'
>>> print(a.join("123")
>>> p = '.'
>>> print(p.join("USA")
1-2-3
U.S.A.
```
As you can see, we could scan a document for instances where it simply said USA, and return it with U.S.A., and other really cool things. One would do well spending a little time to learn how to use this method, it will pay dividends in the future.

```
#lower() -- return lower-case text when there exists upper-case text in a string.
name = 'SHASTEN'
print(name.lower())
```