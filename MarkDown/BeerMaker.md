Beer Maker
==========
## This is going to be a website similar to the famous app and website, Beer Smith 
OBJECTIVES
========
This is going to be a website that allows the user to fill out this form, and get some guidance on the volume of grain, hops, and barley needed to make the different styles of beer that exist. 

Also, I want to create a SQL database that users can store on to the cloud to save all of the batches that the user can pull up at any time.


