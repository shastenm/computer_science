# Which of the following strings is the longest?
# Use the len() function to find out.

longest_german_word = "Donaudampfschifffahrtsgesellschaftskapitänskajütentürschnalle"
longest_hungarian_word = "Megszentségteleníthetetlenségeskedéseitekért"
longest_finnish_word = "Lentokonesuihkuturbiinimoottoriapumekaanikkoaliupseerioppilas"
strong_password = "%8Ddb^ca<*'{9pur/Y(8n}^QPm3G?JJY}\(<bCGHv^FfM}.;)khpkSYTfMA@>N"

german = len(longest_german_word)
hungarian = len(longest_hungarian_word)
finnish = len(longest_finnish_word)
password = len(strong_password)

if german > hungarian and german > finnish and german > password:
    print("German has the most characters.")
elif hungarian > german and hungarian > finnish and hungarian > password:
    print("Hungarian has the most characters.")
elif finnish > german and finnish > hungarian and finnish > password:
    print("Finnish has the most characters.")
else:
    print("The Strong Password has the most characters.")
