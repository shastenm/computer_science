# Use the modulo operator to confirm which of the values
# shown below are divisible by seven.

num_1 = 42
num_2 = 137
num_3 = 455
num_4 = 1997

if num_1 % 7 == 0:
    print("Number 1 divisible by 7.")
if num_2 % 7 == 0:
    print("Number 2 divisible by 7.")
if num_3 % 7 == 0:
    print("Number 3 divisible by 7.")
if num_4 % 7 == 0:
    print("Number 4 divisible by 7.")