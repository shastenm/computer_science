# Write the necessary code calculate the volume and surface area
# of a cylinder with a radius of 3.14 and a height of 5.
# Print out the result.

PI = 3.14
RADIUS = 3.14
HEIGHT = 5.0


s_area = (2 * (PI * (RADIUS ** 2))) + (2 * (PI * RADIUS * (HEIGHT)))
volume = s_area * HEIGHT
print(s_area)
print(volume)    

