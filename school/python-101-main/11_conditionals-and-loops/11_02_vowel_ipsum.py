# Print the total number of vowels that are used in the lorem ipsum text.

lorem_ipsum = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
mollit anim id est laborum."""

lowercase = lorem_ipsum.lower()
vowel_counts = {}

for vowel in 'aeiou':
    count = lowercase.count(vowel)
    vowel_counts[vowel] = count

print(vowel_counts)

counts = vowel_counts.values()
total_values = sum(counts)
print(total_values)
