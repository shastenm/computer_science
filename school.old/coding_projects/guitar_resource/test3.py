#TUNING_INTERVALS


# Resource reference
GUITAR_FRETS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
MUSIC_NOTES = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']*3
SCALE_INTERVAL_LOOKUP = {'MAJOR': [0,2,4,5,7,9,11], 'MINOR':[0,2,3,5,6,7]}

# This is my work around for the fretboard (root = int insid of a list)
class Tunings:
    def __init__(self, tuning):
        self.tuning = tuning

        self.tuning = input("Choose your tuning (S)tandard, (D)rop-d: ").upper()
        

    def play_tuning(self):
        if self.tuning == "S":
            standard_tuning = [ 
               MUSIC_NOTES.index('E'), 
               MUSIC_NOTES.index('B'),
               MUSIC_NOTES.index('G'), 
               MUSIC_NOTES.index('D'), 
               MUSIC_NOTES.index('A'),
               MUSIC_NOTES.index('E'),
            ]
        elif self.tuning == "D":
            drop_d_tuning = [ 
               MUSIC_NOTES.index('D'), 
               MUSIC_NOTES.index('B'),
               MUSIC_NOTES.index('G'), 
               MUSIC_NOTES.index('D'), 
               MUSIC_NOTES.index('A'),
               MUSIC_NOTES.index('D'),
            ]

class StartingNote(Tunings): #this determines if there is a capo in use
    def __init__(self):
        pass
    def open_tuning():
        for i in self.tuning:
            string = self.notes[i:i + 20]
            print(string)
     #       zipped_string = zip(fret, string)
