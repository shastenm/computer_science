#Notes in relation to frets [lists]
fret = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']*3
chromatic = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']*3

# This is my work around for the fretboard (root = int insid of a list)

standard_tuning = [ 
    notes.index('E'), 
    notes.index('B'),
    notes.index('G'), 
    notes.index('D'), 
    notes.index('A'),
    notes.index('E'),
]

#created notes in fretboard
def open_tuning():
    for i in standard_tuning:
        string = notes[i:i + 13]
        print(string)
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
        #    print(string, zipped_string_list)


#capos frets 1-8
def first_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 14]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[0:14])


def second_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 15]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[1:15])


def third_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 16]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[2:16])


def fourth_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 17]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[3:17])


def fifth_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 18]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[4:18])


def sixth_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 19]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[5:19])


def seventh_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 20]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[6:20])


def eighth_fret_capo():
    for i in standard_tuning:
        string = notes[i:i + 21]
        zipped_string = zip(fret, string)
        for string in zipped_string:
            zipped_string_list = list(zipped_string)
            print(zipped_string_list[7:21])


def main():
    use_capo = input("Are you using capo (Y/N? ").upper()
    if use_capo == "Y":
        select_fret = int(input("Which fret do you want the capo on (1-8)? "))
        if select_fret == 1:
            first_fret_capo()
        elif select_fret == 2:
            second_fret_capo()
        elif select_fret == 3:
            third_fret_capo()
        elif select_fret == 4:
            fourth_fret_capo()
        elif select_fret == 5:
            fifth_fret_capo()
        elif select_fret == 6:
            sixth_fret_capo()
        elif select_fret == 7:
            seventh_fret_capo()
        elif select_fret == 8:
            eighth_fret_capo()
    elif use_capo == "N":
        open_tuning()
main()
