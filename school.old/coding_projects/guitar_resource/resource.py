class Scale():
    

    def __init__(self, notes, root, index):
        self.self.notes = self.notes
        self.self.root = self.root
        self.index = index

    def choose_notes(self):
        self.index = 0
        self.notes = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#']
        self.root = input("Choose root: ").upper()

    def choose_chromatic_scale(self):
        scale = [self.notes[(y+self.notes.index(self.root))%len(self.notes)] for y in [0,1,2,3,4,5,6,7,8,9,10,11]]
        print(scale)
    def choose_major_scale(self):
        scale = [self.notes[(y+self.notes.index(self.root))%len(self.notes)] for y in [0,2,4,5,7,9,11]]
        print(scale)
    def choose_nat_har_minor_scale(self):
        scale = [self.notes[(y+self.notes.index(self.root))%len(self.notes)] for y in [0,2,3,5,7,8,11]]
        print(scale)
    def choose_pentatonic_scale(self):
        scale = [self.notes[(y+self.notes.index(self.root))%len(self.notes)] for y in [0,3,5,7,10]]
        print(scale)
    
    def main(self, scale):
        choose_scale = input("""Which Scale Type Do You Want? 
        (C)hromatic, (M)ajor, (N)atural Harmonic Minor, or (Pentatonic)? """).upper()
        if choose_scale == "C":
            self.choose_chromatic_scale(self)
        elif choose_scale == "M":
            self.choose_major_scale(self)
        elif choose_scale == "N":
            self.choose_nat_har_minor_scale(self)
        else:
            self.choose_pentatonic_scale(self)
    
    